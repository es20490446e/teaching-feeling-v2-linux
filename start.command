#!/bin/sh
# (MIT) Alberto Salvia Novella (es20490446e.wordpress.com)

cd "$(dirname "${0}")"

if [ "$(uname)" == "Darwin" ]; then
	./TeachingFeeling.app/Contents/MacOS/nwjs .
else
	nw_linux/nw . &>/dev/null
fi
